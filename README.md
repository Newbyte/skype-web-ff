# Skype Web FF
## What works
At the time of writing (and testing), the following works

- Audio calls, both parties can hear each other
- Chatting
- Notifications, if permission is given in Firefox

## What hasn't been properly tested

- Video calls

## How it works
It changes the user agent to Chrome for all URLs under https://web.skype.com/. That's it. Your user agent remains unmodified for other websites.

## Links
Mozilla Addons listing: https://addons.mozilla.org/sv-SE/firefox/addon/skype-web-ff